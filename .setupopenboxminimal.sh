#!/bin/bash

yay -S tint2 openbox obconf obmenu obkey-gtk3 lxappearance-gtk3 obmenu-generator wpgtk-git termite perl gtk2-perl perl-gtk3

mkdir ~/.icons
mkdir ~/.themes
mkdir ~/.config/openbox
cp /etc/xdg/openbox/* ~/.config/openbox

obmenu-generator -i -p

wpg-install.sh -toig
